const addButton = document.getElementById('addButton')
const cancelButton = document.getElementById('cancelButton')
const saveButton = document.getElementById('saveButton')
const deleteButton = document.getElementById('deleteButton')
const undoButton = document.getElementById('undoButton')

const formModal = document.getElementById('formModal')
const entryInput = document.getElementById('entryInput')
const entriesContainer = document.getElementById('entriesListContainer')

let entries = []
let lastDeletedEntries = []
let lastAddedEntries = []
let isLastActionDelete = false
let selectedEntries = []

function showEntryForm() {
    formModal.style.display = 'block'
}
function hideEntryForm() {
    formModal.style.display = 'none'
}

function showEntriesContainer() {
  entriesContainer.style.display = 'block'
}
function hideEntriesContainer() {
  entriesContainer.style.display = 'none'
}


function updateUI() {
  entriesContainer.innerHTML = '';
  entriesContainer.className = 'entries-list-container'
  entries.forEach((entry, index) => {
    const newEntry = document.createElement('div')
    newEntry.className = 'single-entry';
    newEntry.addEventListener('click', () => {
      if(selectedEntries.includes(entry)) {
        // for the selected entries we should use the index, but to have a quick solution I used the entry itself
        selectedEntries = selectedEntries.filter(selectedEntry =>  !entry === selectedEntry)
      }else{
        selectedEntries.push(entry)
      }
      updateSelectedEntryUI()
    })
    newEntry.addEventListener('dblclick', () => {
      selectedEntries = [entry]
      deleteEntries()
    })

    newEntry.textContent = entry
    entriesContainer.appendChild(newEntry)
  })
}

function updateSelectedEntryUI() {
  entriesContainer.childNodes.forEach((entry, index) => {
    if(selectedEntries.includes(entry.innerText)) {
      entry.classList.add('selected')
    } else {
      entry.classList.remove('selected')
    }
  })
}


function addNewEntry() {
  const newText = entryInput.value.trim()
  if(newText !== '') {
    entries.push(newText)
    lastAddedEntries = [newText]
    entryInput.value = ''
    isLastActionDelete = false
    hideEntryForm()
    showEntriesContainer()
    selectedEntries = []
    updateUI()
  }
}
function deleteEntries(){
  if(selectedEntries.length === 0) return
  let finalEntries = entries.filter(entry => !selectedEntries.includes(entry))
  lastDeletedEntries = selectedEntries
  entries = finalEntries

  isLastActionDelete = true
  if(entries.length === 0) {
    hideEntriesContainer()
  }
  selectedEntries = []
  updateUI()
}
function undoLastAction() {
  if(isLastActionDelete) {
    lastDeletedEntries.forEach((entry) => {
      entries.push(entry)
    })
    lastAddedEntries = lastDeletedEntries 
    lastDeletedEntries = []
    showEntriesContainer()
    isLastActionDelete = false
  }else {
    lastDeletedEntries = lastAddedEntries
    let newEntries = entries.filter(entry => !lastAddedEntries.includes(entry))
    entries = newEntries
    lastAddedEntries = []
    if(entries.length === 0) {
      hideEntriesContainer()
    }
    isLastActionDelete = true
  }
  selectedEntries = []
  updateUI()
}

addButton.addEventListener('click', showEntryForm)
cancelButton.addEventListener('click', hideEntryForm)
saveButton.addEventListener('click', addNewEntry)
deleteButton.addEventListener('click', deleteEntries)
undoButton.addEventListener('click', undoLastAction)
window.addEventListener('click', (event) => {
  if (event.target === formModal) {
    hideEntryForm();
  }
});