import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRotateLeft } from '@fortawesome/free-solid-svg-icons'

import './App.css'
import Dialog from './components/Dialog'
import Button from './components/Button'

function App () {
  const [newInput, setNewInput] = useState('')
  const [dialogIsOpen, setDialogIsOpen] = useState(false)
  const [entries, setEntries] = useState([])
  const [lastDeletedEntries, setLastDeletedEntries] = useState([])
  const [lastAddedEntries, setLastAddedEntries] = useState([])
  const [isLastActionDelete, setIsLastActionDelete] = useState(false)
  const [selectedEntries, setSelectedEntries] = useState([])

  const openDialog = () => {
    setDialogIsOpen(true)
  }
  const closeDialog = () => {
    setDialogIsOpen(false)
  }

  const selectEntry = (entry) => {
    if (selectedEntries.includes(entry)) {
      // for the selected entries we should use the index, but to have a quick solution I used the entry itself
      setSelectedEntries(prevData => prevData.filter(item => item !== entry))
    } else {
      setSelectedEntries([...selectedEntries, entry])
    }
  }

  const addNewEntry = () => {
    const newEntry = newInput.trim()
    if (newEntry === '') return
    setEntries([...entries, newEntry])
    setLastAddedEntries([newEntry])
    setIsLastActionDelete(false)
    setSelectedEntries([])
    closeDialog()
  }

  const deleteEntries = () => {
    if (selectedEntries.length === 0) return
    setLastDeletedEntries(selectedEntries)
    setEntries(entries.filter(entry => !selectedEntries.includes(entry)))
    setLastAddedEntries([])
    setIsLastActionDelete(true)
    setSelectedEntries([])
  }

  const undoLastAction = () => {
    if (isLastActionDelete) {
      const newEntriesList = []
      lastDeletedEntries.forEach(entry => {
        newEntriesList.push(entry)
      })
      setEntries([...entries, ...newEntriesList])
      setLastAddedEntries(lastDeletedEntries)
      setLastDeletedEntries([])
      setIsLastActionDelete(false)
    } else {
      setLastDeletedEntries(lastAddedEntries)
      setEntries(entries.filter(entry => !lastAddedEntries.includes(entry)))
      setLastAddedEntries([])
      setIsLastActionDelete(true)
    }
    setSelectedEntries([])
  }

  return (
    <div className="App">
      <Dialog isOpen={dialogIsOpen} onClose={closeDialog} onSubmit={addNewEntry} onInputChange={setNewInput}></Dialog>
      <div className="container">
        <div className="content">
          <h1>This is a technical proof</h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias sed iusto praesentium impedit. Magni inventore rerum cupiditate voluptatibus dolores repellendus ratione dolore nulla ipsam. Veritatis, quaerat laboriosam. Sunt, iusto inventore.</p>
          <div className="entries-list-container">
            {entries.length !== 0
              ? entries.map((entry, index) => (
              <div key={index} onClick={() => selectEntry(entry)} className={`single-entry ${selectedEntries.includes(entry) ? 'selected' : ''}`}>
                {entry}
              </div>
              ))
              : <p>No entries yet</p>}
          </div>
          <div className="buttons-row">
            <div>
              <Button onClick={undoLastAction} icon={<FontAwesomeIcon icon={faArrowRotateLeft} />}></Button>
              <Button onClick={deleteEntries} text="DELETE"></Button>
            </div>
              <Button onClick={openDialog} isFilled={true} text="ADD"></Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
