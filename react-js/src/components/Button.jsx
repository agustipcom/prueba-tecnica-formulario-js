import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ text, icon, onClick, isFilled, haveMargin }) => {
  return (
    <button
      onClick={onClick}
      className={`${isFilled ? 'filled' : ''} ${haveMargin ? 'with-margin' : ''}`}
    >
      {icon && icon}
      {text && text}
    </button>
  )
}

Button.propTypes = {
  text: PropTypes.string,
  icon: PropTypes.node,
  onClick: PropTypes.func,
  isFilled: PropTypes.bool,
  haveMargin: PropTypes.bool
}

export default Button
