import React from 'react'
import PropTypes from 'prop-types'
import Button from './Button'

const Dialog = ({ isOpen, onClose, onSubmit, onInputChange }) => {
  const handleOutsideClick = (event) => {
    // Si el usuario hace clic en el fondo oscuro del diálogo, cerramos el diálogo.
    if (event.target.classList.contains('form-modal')) {
      onClose()
    }
  }
  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      onSubmit()
    }
  }
  const handleInputChange = (event) => {
    onInputChange(event.target.value)
  }

  if (!isOpen) return null

  return (
    <div className="form-modal" onClick={handleOutsideClick}>
      <div className="form-modal-content">
          <p>Add item to list</p>
          <input type="text" onChange={handleInputChange} onKeyDown={handleKeyDown} placeholder="Type the text here..." ></input>
          <div className="buttons-row align-right">
            <Button onClick={onSubmit} isFilled={true} haveMargin={true} text={'ADD'}></Button>
            <Button onClick={onClose} haveMargin={true} text={'CANCEL'}></Button>
          </div>
      </div>
    </div>
  )
}

Dialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired
}

export default Dialog
